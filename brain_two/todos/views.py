from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.form import TodoForm, TodoItemForm


def show_all_lists(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list_list": lists,
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todos")
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_detail", id=list.id)
    else:
        form = TodoForm(instance=list)
    context = {"todo_list": list, "form": form}
    return render(request, "todos/update.html", context)


def delete_todo_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todos")

    return render(request, "todos/delete.html")
