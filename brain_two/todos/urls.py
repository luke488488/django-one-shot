from django.urls import path
from todos.views import show_all_lists, todo_list_detail, todo_list_create
from todos.views import update_todo_list, delete_todo_list

urlpatterns = [
    path("", show_all_lists, name="todos"),
    path("<int:id>/", todo_list_detail, name="todo_detail"),
    path("create/", todo_list_create, name="todo_create"),
    path("update/<int:id>", update_todo_list, name="todo_update"),
    path("delete/<int:id>", delete_todo_list, name="delete_todo"),
]
